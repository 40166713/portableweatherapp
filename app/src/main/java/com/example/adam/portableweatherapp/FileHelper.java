package com.example.adam.portableweatherapp;

import android.content.Context;
import android.support.v7.app.ActionBarActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by adam on 28/02/15.
 * Method that saves String or Gson objects in the internal file and reads it back,
 */
public class FileHelper extends ActionBarActivity {
    private final String Filename;
    private final Context MyContext;
    FileHelper(String filename, Context context){
        Filename = filename;
        MyContext = context;
    }

    //method to append to file, without deleting it
    public void appendToFile(String outString){
        FileOutputStream fileOutputStream;
        try {
            fileOutputStream = MyContext.openFileOutput(Filename, MODE_APPEND);
            fileOutputStream.write(outString.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //remove file if exists, create new file, holds only one instance of the object
    public void writeToFile(String outString) {
        FileOutputStream fileOutputStream;
        try {
            if (fileExistence(Filename)) {
                MyContext.deleteFile(Filename);
            }
            fileOutputStream = MyContext.openFileOutput(Filename, Context.MODE_PRIVATE);
            fileOutputStream.write(outString.getBytes());
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //read from file, return String
    public String readFromFile(String filename) {
        int ch;
        StringBuffer inString = new StringBuffer("");
        FileInputStream fileInputStream;

        try {
            fileInputStream = MyContext.openFileInput(filename);
            try {
                while ((ch = fileInputStream.read()) != -1) {
                    inString.append((char) ch);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            fileInputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return inString.toString();
    }

    //check if file exists
    public boolean fileExistence(String fname) {
        File file = MyContext.getFileStreamPath(fname);
        return file.exists();
    }
}
