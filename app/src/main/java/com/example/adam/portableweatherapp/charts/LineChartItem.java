package com.example.adam.portableweatherapp.charts;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;

import com.example.adam.portableweatherapp.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.ChartData;
import com.github.mikephil.charting.data.LineData;

/**
 * Created by adam on 01/03/15.
 * Line chart item class that sets up line chart view and styling
 * Adapted from MPAndroidCharts
 */
public class LineChartItem extends ChartItem {

    private final Typeface mTf;
    private final String Title;

    //constructor
    public LineChartItem(ChartData<?> cd, Context context, String title) {
        super(cd);
        mTf = Typeface.createFromAsset(context.getAssets(), "OpenSans-Regular.ttf");
        Title = title;
    }

    @Override
    public int getItemType() {
        return TYPE_LINECHART;
    }

    @Override
    public View getView(int position, View convertView, Context context) {

        ViewHolder holder = null;

        if (convertView == null){
            holder = new ViewHolder();

            convertView = LayoutInflater.from(context).inflate(
                    R.layout.list_item_linechart, null);
            holder.chart = (LineChart) convertView.findViewById(R.id.chart);

            convertView.setTag(holder);
        }
        else{
            holder = (ViewHolder)convertView.getTag();
        }

        // apply styling
        holder.chart.setDescription(Title);
        holder.chart.setDrawGridBackground(false);
        holder.chart.setPinchZoom(true);
        holder.chart.setHighlightEnabled(true);
        holder.chart.setDragEnabled(true);
        holder.chart.setHighlightIndicatorEnabled(true);

        //set up X axis
        XAxis xAxis = holder.chart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTypeface(mTf);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(true);

        //set up Y left axis
        YAxis leftAxis = holder.chart.getAxisLeft();
        leftAxis.setTypeface(mTf);
        leftAxis.setLabelCount(5);
        leftAxis.setStartAtZero(false);

        //set up Y right axis
        YAxis rightAxis = holder.chart.getAxisRight();
        rightAxis.setTypeface(mTf);
        rightAxis.setLabelCount(5);
        rightAxis.setDrawGridLines(false);
        rightAxis.setStartAtZero(false);

        // set data
        holder.chart.setData((LineData) mChartData);

        //animate chart on load
        holder.chart.animateX(750);

        return convertView;
    }

    private static class ViewHolder {
        LineChart chart;
    }
}
