package com.example.adam.portableweatherapp;

import android.location.Location;



import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileInputStream;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by adam on 02/03/15.
 *
 * Adapted from
 *          http://android-coding.blogspot.co.uk/2013/01/get-latitude-and-longitude-from-gpx-file.html
 *
 * Method that reads gpx file and searches for specific tags that contain latitude and longitude
 * returns List with locations
 */
class GPX_Parser {

    public static final String TRKPT = "trkpt";
    public static final String RTEPT = "rtept";
    private static final String LATITUDE = "lat";
    private static final String LONGITUDE = "lon";


    GPX_Parser(){

    }

    //method that iterates through the file and gets only Location(Lat,Long) into the list
    public List<Location> decodeGPX(File file, String format){
        List<Location> list = new ArrayList<Location>();

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();

        try{
            DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
            FileInputStream fileInputStream = new FileInputStream(file);
            Document document = documentBuilder.parse(fileInputStream);
            Element elementRoot = document.getDocumentElement();

            NodeList nodeList_trkpt = elementRoot.getElementsByTagName(format);

            for (int i = 0; i < nodeList_trkpt.getLength(); i++){
                Node node = nodeList_trkpt.item(i);
                NamedNodeMap attributes = node.getAttributes();

                String newLatitude = attributes.getNamedItem(LATITUDE).getTextContent();
                Double newLatitudeDouble = Double.parseDouble(newLatitude);

                String newLongitude = attributes.getNamedItem(LONGITUDE).getTextContent();
                Double newLongitudeDouble = Double.parseDouble(newLongitude);

                String newLocationName = newLatitude + ":" + newLongitude;
                Location newLocation = new Location(newLocationName);
                newLocation.setLatitude(newLatitudeDouble);
                newLocation.setLongitude(newLongitudeDouble);

                list.add(newLocation);
            }
            fileInputStream.close();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        return list;
    }
}
