/**
 * Rotation Refresh Icon:
 * http://androidblog.reindustries.com/animating-update-or-loading-refresh-actionbar-button/
 *
 * Weather Forecast functionality and icons based on https://github.com/grgr07/Simple-Weather-App
 * RemoteFetch taken from https://github.com/grgr07/Simple-Weather-App
 *
 * Update 11/03/15 Adds setTextViewDefault() to on post return == null and on icon refresh click
 * when cache empty
 **/


package com.example.adam.portableweatherapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Created by adam on 26/02/15
 * Forecast activity allows user to see weather forecast for a chosen city
 * it uses search view in toolbar to get a city from Open Weather Map servers
 * refresh button allows to fetch data for the same city that is displayed
 * Remote Fetch class deals with server connection
 */
public class Forecast extends BaseClass {

    //forecast object to hold data values and save them on internal storage
    private static ForecastObjects forecastObjects;
    //string in Json format to hold values
    private String jsonGson;
    //handler to update views while in different thread
    private Handler handler;

    //UI variables
    private String[] mDrawerItems;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private Typeface weatherFont;
    private SearchView searchView;
    private MenuItem searchItem;
    private TextView tvCity;
    private TextView tvTemperature;
    private TextView tvPressure;
    private TextView tvHumidity;
    private TextView weatherIcon;
    private TextView lastUpdated;

    //tags and filename
    private static final String FORECAST_PREF = "ForecastPref";
    private static final String CURRENT_CITY = "CURRENT_CITY";
    private static final String FILENAME = "forecast_file";

    //selected item to check drawer clicked item
    private String selectedItem;
    //shared preferences to store city name
    private SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set up toolbar
        setToolbarTitle(getString(R.string.forecast));
        setActionBarIcon(R.mipmap.ic_menu_white_48dp);
        //instantiate handler
        handler = new Handler();

        /************************************* UI SET UP*****************************************/
        tvCity = (TextView) findViewById(R.id.forecast_tv_city);
        tvTemperature = (TextView) findViewById(R.id.forecast_temperature_data);
        tvPressure = (TextView) findViewById(R.id.forecast_pressure_data);
        tvHumidity = (TextView) findViewById(R.id.forecast_humidity_data);
        weatherIcon = (TextView) findViewById(R.id.forecast_weather_icon);
        lastUpdated = (TextView) findViewById(R.id.forecast_last_updated);

        weatherFont = Typeface.createFromAsset(this.getAssets(), "weather.ttf");
        weatherIcon.setTypeface(weatherFont);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        //code for list view and drawer items
        ListView mDrawerList = (ListView) findViewById(R.id.drawer_list);
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.setDrawerShadow(R.color.colorPrimaryDark, Gravity.START);


        mDrawerItems = getResources().getStringArray(R.array.drawer_items_array);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.item_row, mDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,                           /* host Activity */
                drawer,                         /* DrawerLayout object */
                toolbar,                       /* nav drawer icon to replace 'Up' caret */
                R.string.open_drawer,                          /* "open drawer" description */
                R.string.close_drawer                           /* "close drawer" description */
        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

        };

        // Set the drawer toggle as the DrawerListener
        drawer.setDrawerListener(mDrawerToggle);


        /************************************* UI SET UP FROM FILE*****************************************/
        //shared preferences for weather location
        sharedPref = this.getSharedPreferences(FORECAST_PREF, Context.MODE_PRIVATE);

        FileHelper fileHelper= new FileHelper(FILENAME, this);
        //if file exists update views
        if (fileHelper.fileExistence(FILENAME)){
            jsonGson = fileHelper.readFromFile(FILENAME);
            if (jsonGson.length() < 10) {
                //file error - do not update views
            } else {
                //read file to ForecastObjects class using Gson
                Gson gson = new Gson();
                forecastObjects = gson.fromJson(jsonGson, ForecastObjects.class);
                tvCity.setText(forecastObjects.City);
                tvHumidity.setText(forecastObjects.Humidity);
                tvPressure.setText(forecastObjects.Pressure);
                tvTemperature.setText(forecastObjects.Temperature + " ℃");
                setWeatherIcon(forecastObjects.ActualId, forecastObjects.Sunrise, forecastObjects.Sunset);
                lastUpdated.setText("Last update: " + forecastObjects.LastUpdate
                        + "\n" +
                        "Last check: " + forecastObjects.CheckedOn);
            }
        }
    }

    //method that calls static getJSON() from RemoteFetch to connect to server and retrieve Json object
    private void updateWeatherData(final String city) {
        new Thread() {
            public void run() {
                final JSONObject json = RemoteFetch.getJSON(getApplicationContext(), city);
                try {
                    //server error - notify user
                    if (json == null) {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(Forecast.this, "No response - server may be down", Toast.LENGTH_LONG).show();
                            }
                        });
                    //if response not OK assume user typed wrong city, or city not in the database
                    } else if (json.getInt("cod") != 200){
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                setTextViewDefault(getString(R.string.city_not_found));
                            }
                        });
                    }
                    //response OK, update data
                    else {
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                renderWeather(json);
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    //method that retrieves specific values from Json object send over from OWM server
    private void renderWeather(JSONObject json) {
        try {
            tvCity.setText(json.getString("name").toUpperCase(Locale.ENGLISH) +
                    ", " +
                    json.getJSONObject("sys").getString("country"));
            JSONObject details = json.getJSONArray("weather").getJSONObject(0);
            JSONObject main = json.getJSONObject("main");

            //save last searched city using shared preferences
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(CURRENT_CITY, json.getString("name").toUpperCase(Locale.ENGLISH));
            editor.apply();

            //set weather values
            tvTemperature.setText(String.format("%.2f", main.getDouble("temp")) + " ℃");
            tvPressure.setText(main.getString("pressure") + "hPa");
            tvHumidity.setText(main.getString("humidity") + "%");

            //set weather icon
             setWeatherIcon(details.getInt("id"),
                    json.getJSONObject("sys").getLong("sunrise") * 1000,
                    json.getJSONObject("sys").getLong("sunset") * 1000);

            //get timestamp for both last update and last check
            DateFormat df = DateFormat.getDateTimeInstance();
            String checkedOn = df.format(new Date());
            String updatedOn = df.format(new Date(json.getLong("dt")*1000));
            lastUpdated.setText("Last update: " + updatedOn
                    + "\n" +
                    "Last check: " + checkedOn);

            //assign values to the ForecastObject
            forecastObjects = new ForecastObjects(
                    tvCity.getText().toString(),                            //city name
                    main.getString("pressure") + " hPa",                    //pressure
                    main.getString("humidity") + " %",                      //humidity
                    String.format("%.2f", main.getDouble("temp")),          //temperature
                    details.getInt("id"),                                   //id
                    json.getJSONObject("sys").getLong("sunrise") * 1000,    //sunrise
                    json.getJSONObject("sys").getLong("sunset") * 1000,     //sunset
                    updatedOn,                                              //last update
                    checkedOn                                               //checked on
            );

            //save object using Gson to Json string and write the string into the file
            Gson gson = new Gson();
            jsonGson = gson.toJson(forecastObjects);
            FileHelper fileHelper = new FileHelper(FILENAME, this);
            fileHelper.writeToFile(jsonGson);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //set weather icon
    // github.com/grgr07/Simple-Weather-App
    private void setWeatherIcon(int actualId, long sunrise, long sunset) {
        int id = actualId / 100;
        String icon = "";
        if (actualId == 800) {
            long currentTime = new Date().getTime();
            if (currentTime >= sunrise && currentTime < sunset) {
                icon = this.getString(R.string.weather_sunny);
            } else {
                icon = this.getString(R.string.weather_clear_night);
            }
        } else {
            switch (id) {
                case 2:
                    icon = this.getString(R.string.weather_thunder);
                    break;
                case 3:
                    icon = this.getString(R.string.weather_drizzle);
                    break;
                case 7:
                    icon = this.getString(R.string.weather_foggy);
                    break;
                case 8:
                    icon = this.getString(R.string.weather_cloudy);
                    break;
                case 6:
                    icon = this.getString(R.string.weather_snowy);
                    break;
                case 5:
                    icon = this.getString(R.string.weather_rainy);
                    break;
            }
        }
        weatherIcon.setText(icon);

    }

    //update weather on city change
    void changeCity(String city) {
        updateWeatherData(city);
    }

    //set content view
    @Override
    protected int getLayoutResource() {
        return R.layout.forecast;
    }

    //set up menu in the toolbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_forecast_search, menu);

        searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                //do something
                return true;
            }
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                //do something
                return true;
            }
        });

        //listener for search view
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            //update city on query submit
            @Override
            public boolean onQueryTextSubmit(String s) {
                String citySearch = searchView.getQuery().toString();
                searchItem.collapseActionView();
                changeCity(citySearch);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
            //fetch weather data
            case R.id.action_refresh:
                 item.setActionView(animateRefreshIcon());
                 updateWeatherData(tvCity.getText().toString());
                 Log.d("FORECAST", "updating weather");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    /**
     * Listener for drawer, same for all activities. In later projects use FragmentLayout instead
     * of this solution.
     **/
    private class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            drawer.closeDrawer(Gravity.START);                      //close drawer
            selectedItem = mDrawerItems[position];                  //get string from selected drawer item

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //if-else statement to launch other activities
                    if (selectedItem.equalsIgnoreCase(getString(R.string.real_time_data))) {
                        //do nothing here at the moment
                        Intent intent = new Intent(Forecast.this, PWSWeather.class);
                        startActivity(intent);
                        Forecast.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.forecast))) {
                        //do nothing here at the moment
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.graphs))) {
                        Intent intent = new Intent(Forecast.this, Graphs.class);
                        startActivity(intent);
                        Forecast.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.map))) {
                        Intent intent = new Intent(Forecast.this, MyMap.class);
                        startActivity(intent);
                        Forecast.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.load_route))) {
                        Intent intent = new Intent(Forecast.this, LoadRoute.class);
                        startActivity(intent);
                        Forecast.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.about))) {
                        Intent intent = new Intent(Forecast.this, About.class);
                        startActivity(intent);
                    }
                }
            }, 300);
        }
    }

    //private class to hold weather object values and restore them on create
    private class ForecastObjects {
        final String City;
        final String Pressure;
        final String Humidity;
        final String Temperature;
        final String LastUpdate;
        final String CheckedOn;
        final int ActualId;
        final long Sunrise;
        final long Sunset;

        ForecastObjects(String city, String pressure, String humidity, String temperature, int actualId,
                        long sunrise, long sunset, String lastUpdate, String checkedOn) {
            City = city;
            Pressure = pressure;
            Humidity = humidity;
            Temperature = temperature;
            ActualId = actualId;
            Sunrise = sunrise;
            Sunset = sunset;
            LastUpdate = lastUpdate;
            CheckedOn = checkedOn;
        }
    }

    //animate refresh icon
    private ImageView animateRefreshIcon(){
        //do animation start
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageView imageView = (ImageView) layoutInflater.inflate(R.layout.iv_refresh, null);
        Animation rotation = AnimationUtils.loadAnimation(this, R.anim.rotate_refresh);
        rotation.setRepeatCount(1);
        imageView.startAnimation(rotation);
        return imageView;
    }

    //set UI elements to default
    private void setTextViewDefault(String textViewDefault){
        tvCity.setText(textViewDefault);
        tvTemperature.setText(getApplicationContext().getString(R.string.no_data_dash));
        tvHumidity.setText(getApplicationContext().getString(R.string.no_data_dash));
        tvPressure.setText(getApplicationContext().getString(R.string.no_data_dash));
        weatherIcon.setText("");
    }

}