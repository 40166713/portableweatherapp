package com.example.adam.portableweatherapp;

import android.content.Context;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by adam on 27/02/15.
 * Class that provides a service of connecting to Open Weather Map server and fetches weather data
 * based on the supplied City
 */
class RemoteFetch {

    //server address
    private static final String OPEN_WEATHER_MAP_API =
            "http://api.openweathermap.org/data/2.5/weather?q=%s&units=metric";

    //static method to get Json object
    public static JSONObject getJSON(Context context, String city) {
        try {
            URL url = new URL(String.format(OPEN_WEATHER_MAP_API, city));
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            //using API key provided upon registration
            connection.addRequestProperty("x-api-key",
                    context.getString(R.string.open_weather_map_api_key));

            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            StringBuffer json = new StringBuffer(1024);
            String tmp = "";
            //read data
            while ((tmp = bufferedReader.readLine()) != null) {
                json.append(tmp).append("\n");
            }
            //close buffer
            bufferedReader.close();

            //assign Json object
            JSONObject data = new JSONObject(json.toString());

            // This value will be 404 if the request was not successful
            if (data.getInt("cod") != 200) {
                if (data.getInt("cod") == 404){
                    return data;
                }else{
                    return null;
                }
            }
            return data;

        } catch (Exception e) {
            return null;
        }
    }
}