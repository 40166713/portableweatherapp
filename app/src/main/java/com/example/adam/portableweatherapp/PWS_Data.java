package com.example.adam.portableweatherapp;

import android.annotation.SuppressLint;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by adam on 05/03/15.
 *
 * Update 11/03/15 Adds getter for properties
 */
public class PWS_Data {
    private String Temperature;
    private String Humidity;
    private String Pressure;
    private String Altitude;
    private String Latitude;
    private String Longitude;
    private Timestamp myTimestamp;
    public static String PWS_DATA_FILENAME = "pws_data_filename";
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat dayMonthFormat = new SimpleDateFormat("dd/MMM");
    @SuppressLint("SimpleDateFormat")
    public static final SimpleDateFormat hoursFormat = new SimpleDateFormat("HH:mm");

    PWS_Data(String temperature, String humidity, String pressure, String altitude, String latitude,
            String longitude){
        Temperature = temperature;
        Humidity = humidity;
        Pressure = pressure;
        Altitude = altitude;
        Latitude = latitude;
        Longitude = longitude;
        myTimestamp = getTimestamp();
    }

    //getters for properties
    public String getTemperature(){
        return this.Temperature;
    }
    public String getHumidity(){
        return this.Humidity;
    }
    public String getPressure(){
        return this.Pressure;
    }
    public String getAltitude(){
        return this.Altitude;
    }
    public String getLatitude(){
        return this.Latitude;
    }
    public String getLongitude(){
        return this.Longitude;
    }
    public Timestamp getMyTimestamp(){
        return this.myTimestamp;
    }

    public static String timestampToHours(Timestamp timestamp){
        if (timestamp == null){
            return null;
        }else{
            return hoursFormat.format(timestamp);
        }
    }
    public static String timestampToDayMonth(Timestamp timestamp){
        if (timestamp == null){
            return null;
        }else{
            return dayMonthFormat.format(timestamp);
        }
    }
    public static Timestamp  getTimestamp(){
        Date date = new Date();
        return new Timestamp(date.getTime());
    }

    /** Methods to cut the data into pieces and retrieve specific values**/
    public static String getTemperatureFromData(String data){
        int index = data.indexOf('P');
        double temperature = Double.parseDouble(data.substring(1, index));
        return String.format("%.0f", temperature);
    }
    public static String getPressureFromData(String data){
        int pressureIndex= data.indexOf('P');
        int altitudeIndex= data.indexOf('A');
        double pressure = Double.parseDouble(data.substring(pressureIndex + 1, altitudeIndex))/100;
        return String.format("%.2f", pressure);
    }
    public static String getAltitudeFromData(String data){
        int humidityIndex = data.indexOf('H');
        int altitudeIndex = data.indexOf('A');
        return data.substring(altitudeIndex + 1, humidityIndex);
    }
    public static String getHumidityFromData(String data){
        int humidityIndex = data.indexOf('H');
        double humidity = Double.parseDouble(data.substring(humidityIndex+1));
        return String.format("%.2f", humidity);
    }
}
