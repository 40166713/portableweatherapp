package com.example.adam.portableweatherapp;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by adam on 10/03/15.
 * Location Provider class based on Google Play location services tutorial
 * connects to the google services and gets location
 * This is new and preferred way of getting devices location recommended by Google
 */
public class LocationProvider implements com.google.android.gms.location.LocationListener,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleApiClient.ConnectionCallbacks {

    //tag
    private static final String TAG = "locationProvider";

    //intervals setup
    private static final long ONE_MINUTE = 1000 * 60;
    private static final long INTERVAL = ONE_MINUTE * 15;
    private static final long FASTES_INTERVAL = ONE_MINUTE;
    private static final long REFRESH_TIME = ONE_MINUTE * 10;
    private static final float ACCURACY = 50.0f;

    Context context;

    //google api variables
    private LocationRequest locationRequest;
    private GoogleApiClient googleApiClient;
    private Location location;
    private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;

    FusedLocationReceiver locationReceiver = null;

    //constructor
    public LocationProvider(Context context, FusedLocationReceiver locationReceiver){
        this.locationReceiver = locationReceiver;
        this.context = context;
        //initialise location request
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        locationRequest.setInterval(INTERVAL);
        locationRequest.setFastestInterval(FASTES_INTERVAL);

        //initialise google API client with location services
        googleApiClient = new GoogleApiClient.Builder(context)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        if (googleApiClient != null){
            googleApiClient.connect();
        }
    }

    //on connected get location based on the refresh time
    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG, "connected");
        Location currentLocation = fusedLocationProviderApi.getLastLocation(googleApiClient);
        if (currentLocation != null && currentLocation.getTime() > REFRESH_TIME){
            location = currentLocation;
        }else{
            fusedLocationProviderApi.requestLocationUpdates(googleApiClient, locationRequest, (com.google.android.gms.location.LocationListener) this);
            // Schedule a Thread to unregister location listeners
            Executors.newScheduledThreadPool(1).schedule(new Runnable() {
                @Override
                public void run() {
                    fusedLocationProviderApi.removeLocationUpdates(googleApiClient,  LocationProvider.this);
                }
            }, ONE_MINUTE, TimeUnit.MILLISECONDS);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "Location changed");
        //if the existing location is empty or
        //the current location accuracy is greater than existing accuracy
        //then store the current location
        if (null == this.location || location.getAccuracy() < this.location.getAccuracy() ){
            this.location = location;
            //inform client class
            locationReceiver.onLocationChanged();
            //if the accuracy is not better, remove all location updates for this listener
            if (this.location.getAccuracy() < ACCURACY){
                fusedLocationProviderApi.removeLocationUpdates(googleApiClient,  this);
            }
        }
    }

    //getter for location
    public Location getLocation(){
        return this.location;
    }

    //if failed to connect - disconnect google api client
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        googleApiClient.disconnect();
    }

}
