/**
 * Matriculation 40166713
 * Created by adam on 26/02/15.
 * PWSWeather is the main activity that starts up when application is first launched
 * It displays data fetched from sensors in text views and provides notification status underneath
 * Connect/Disconnect button allows for bluetooth connection and communication with JY-MCU connected
 * to Arduino board
 * This class doesn't connect to Arduino, onClick only checks if bluetooth adapter is available
 * and fires up intent to enable bluetooth if not enabled yet
 **/


package com.example.adam.portableweatherapp;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class PWSWeather extends BaseClass implements View.OnClickListener{

    //Response receiver that gets data from Service and displays it onto the text views
    private ResponseReceiver receiver;
    //Log Tag
    private static final String PWSWEATHER = "PWSWEATHER";

    //Bluetooth attributes - only to verify bluetooth exists and enabled
    static BluetoothAdapter bluetoothAdapter;
    //boolean to verify if connect button has been clicked to change its state
    static boolean clicked_connect= false;
    //string to verify which item in the drawer has been clicked
    private String selectedItem;
    //boolean to resolve connection
    static boolean connected = false;

    //UI elements
    private String[] mDrawerItems;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private Button connectButton;
    private TextView notificationsText;
    private TextView temperatureTV;
    private TextView humidityTV;
    private TextView pressureTV;
    private TextView altitudeTV;
    MenuItem calibrateIcon;

    //final static constants for onSaveInstanceState
    static final String CLICKED_CONNECT = "click_connect";
    //handler to update UI elements while running different threads
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set up toolbar
        setToolbarTitle(getString(R.string.real_time_data));
        setActionBarIcon(R.mipmap.ic_menu_white_48dp);
        //instantiate handler
        handler = new Handler();
        //instantiate response receiver with filter
        IntentFilter filter = new IntentFilter(ResponseReceiver.ACTION_RESP);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        receiver = new ResponseReceiver();
        registerReceiver(receiver, filter);

        /************************************* UI SET UP*****************************************/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        connectButton = (Button) findViewById(R.id.PWS_connect_button);
        notificationsText = (TextView) findViewById(R.id.PWS_notifications_text);
        temperatureTV = (TextView) findViewById(R.id.PWS_temperature_data);
        pressureTV = (TextView) findViewById(R.id.PWS_pressure_data);
        humidityTV = (TextView) findViewById(R.id.PWS_humidity_data);
        altitudeTV = (TextView) findViewById(R.id.PWS_altitude_data);

        //id saved instance state is not null then get data and set it
        if(savedInstanceState !=  null){
            //get button state and boolean and set it appropriately
            clicked_connect = savedInstanceState.getBoolean(CLICKED_CONNECT);
            if (clicked_connect) {
                connectButton.setText(getString(R.string.disconnect_button_txt));
                connectButton.setTextColor(Color.RED);
            }
        }
        //code for list view and drawer items
        ListView mDrawerList = (ListView) findViewById(R.id.drawer_list);
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.setDrawerShadow(R.color.colorPrimaryDark, Gravity.START);
        mDrawerItems = getResources().getStringArray(R.array.drawer_items_array);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.item_row, mDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,                                           /* host Activity */
                drawer,                                         /* DrawerLayout object */
                toolbar,                                        /* nav drawer icon to replace 'Up' caret */
                R.string.open_drawer,                           /* "open drawer" description */
                R.string.close_drawer                           /* "close drawer" description */
        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };
        // Set the drawer toggle as the DrawerListener
        drawer.setDrawerListener(mDrawerToggle);


        /**************************BLUETOOTH SET UP*****************************************/
        //get default bt adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    }

    //on destroy unregister receiver to avoid conflicts
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);

    }


    /** Get Data from the service and update views **/
    public class ResponseReceiver extends BroadcastReceiver {
        public static final String ACTION_RESP =
                "com.example.adam.portableweatherapp.intent.action.MESSAGE_PROCESSED";

        @Override
        public void onReceive(Context context, Intent intent) {

            try{
                //check first if an error occurred - change UI elements accordingly
                boolean error_received = intent.getBooleanExtra(PWS_Bluetooth.PARAM_BOOL_OUT, false);
                if (error_received){
                    notificationsText.setText("Connection Lost");
                    connectButton.setText(getString(R.string.connect));
                    connectButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                }//otherwise update text views with data
                else{
                    String text = intent.getStringExtra(PWS_Bluetooth.PARAM_OUT_MSG);
                    notificationsText.setText("Connected" + "\n"
                            + "Last update: "
                            + new SimpleDateFormat("HH:mm:ss", Locale.UK).format(new Date()));
                    temperatureTV.setText(PWS_Data.getTemperatureFromData(text) + " ℃");
                    pressureTV.setText(PWS_Data.getPressureFromData(text) + " hPa");
                    altitudeTV.setText(PWS_Data.getAltitudeFromData(text) + " m");
                    humidityTV.setText(PWS_Data.getHumidityFromData(text) + " %");
                }

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    //private class that starts the service on button click or on Activity Result
    private class OpenBTThread extends Thread {
        @Override
        public void run() {
            Log.d("BLUETOOTH", "inside openBT");
            try {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(PWSWeather.this, PWS_Bluetooth.class);
                        intent.putExtra(CLICKED_CONNECT, clicked_connect);
                        startService(intent);
                    }
                }, 5000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //method that checks bluetooth state upon  button click
    private void checkBTState() {
        //if bt not supported inform the user
        if (bluetoothAdapter == null){
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
            clicked_connect = false;
            notificationsText.setText("Bluetooth not supported");
        }
        //if available, check if enabled, if not ask user to enable
        else{
            if (bluetoothAdapter.isEnabled()){
                Log.d(PWSWEATHER, "Bluetooth ON");
                connectButton.setText(getString(R.string.disconnect_button_txt));
                connectButton.setTextColor(Color.RED);
                //connect to PWS

                        Log.d(PWSWEATHER, "OpenBTThread");
                        new OpenBTThread().start();
            }
            else{
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, 1);
            }
        }
    }

    //on bluetooth enabled start service
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1){
            if (bluetoothAdapter.isEnabled()){
                Log.d(PWSWEATHER, "Bluetooth ON - Activity Result");
                connectButton.setText(getString(R.string.disconnect_button_txt));
                connectButton.setTextColor(Color.RED);
                //connect to PWS
                        Log.d(PWSWEATHER, "OpenBTThread");
                        new OpenBTThread().start();
            }
        }
    }

    //set content view
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_pwsweather;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    //on connect/disconnect button click
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.PWS_connect_button:
                if (clicked_connect){
                    //disconnect
                    clicked_connect = false;
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            connectButton.setText(getString(R.string.connect));
                            connectButton.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
                            notificationsText.setText("Bluetooth closed");
                        }
                    }, 300);
                }
                else{
                    //connect
                    clicked_connect = true;
                    notificationsText.setText("Connecting...");
                    checkBTState();
                }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    //drawer listener to response to click and start new activity
    private class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            drawer.closeDrawer(Gravity.START);                      //close drawer
            selectedItem = mDrawerItems[position];                  //get string from selected drawer item

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //if-else statement to launch other activities
                    if (selectedItem.equalsIgnoreCase(getString(R.string.real_time_data))){
                        //do nothing here at the moment
                    }
                    else if (selectedItem.equalsIgnoreCase(getString(R.string.forecast))){
                        Intent intent = new Intent(PWSWeather.this, Forecast.class);
                        startActivity(intent);

                    }
                    else if (selectedItem.equalsIgnoreCase(getString(R.string.graphs))){
                        Intent intent = new Intent(PWSWeather.this, Graphs.class);
                        startActivity(intent);
                    }
                    else if (selectedItem.equalsIgnoreCase(getString(R.string.map))){
                        Intent intent = new Intent(PWSWeather.this, MyMap.class);
                        startActivity(intent);
                    }
                    else if (selectedItem.equalsIgnoreCase(getString(R.string.load_route))){
                        Intent intent = new Intent(PWSWeather.this, LoadRoute.class);
                        startActivity(intent);
                    }
                    else if (selectedItem.equalsIgnoreCase(getString(R.string.about))){
                        Intent intent = new Intent(PWSWeather.this, About.class);
                        startActivity(intent);
                    }
                }
            }, 300);

        }
    }

    //save button state
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //save state of the data
        outState.putBoolean(CLICKED_CONNECT, clicked_connect);

        super.onSaveInstanceState(outState);
    }

    //restore button state
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //get button state and boolean and set it appropriately
        clicked_connect = savedInstanceState.getBoolean(CLICKED_CONNECT);
        if (clicked_connect) {
            connectButton.setText(getString(R.string.disconnect_button_txt));
            connectButton.setTextColor(Color.RED);
        }
    }

    //set button state accordingly
    @Override
    protected void onResume() {
        super.onResume();
        if (clicked_connect) {
            connectButton.setText(getString(R.string.disconnect_button_txt));
            connectButton.setTextColor(Color.RED);
        }
    }

    //inflate options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pwsweather, menu);
        calibrateIcon = (MenuItem) findViewById(R.id.action_calibrate);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.action_calibrate:
                if (!connected){
                    //dialog menu that connection needed
                    notifyAboutRequiredConnection();
                }else{
                    //open up dialog to calibrate
                    getCalibrationValue();
                }
                return true;
            default:
                //nothing
        }
        return super.onOptionsItemSelected(item);
    }

    private void getCalibrationValue() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.calibrate_altitude))
                .content("Please enter your approximate altitude.")
                .inputType(InputType.TYPE_CLASS_NUMBER)
                .positiveText(R.string.ok)
                .negativeText(R.string.cancel)
                .input(getString(R.string.meters), null, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(MaterialDialog materialDialog, CharSequence charSequence) {
                        //do something here
                    }
                }).callback(new MaterialDialog.ButtonCallback() {
            @Override
            public void onPositive(MaterialDialog dialog) {
                super.onPositive(dialog);
                if (dialog.getInputEditText() != null) {
                    String value = String.valueOf((dialog.getInputEditText().getText()));
                    sendCalibrationValue(value);
                }
            }
        })
        .show();
    }

    private void notifyAboutRequiredConnection() {
        new MaterialDialog.Builder(this)
                .title(getString(R.string.calibrate_altitude))
                .content(getString(R.string.calibrate_altitude_not_connected))
                .positiveText(getString(R.string.ok))
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                    }
                }).show();
    }

    private void sendCalibrationValue(String value){
        byte[] bytes = value.getBytes();
        if (PWS_Bluetooth.mmOutputStream!=null){
            try {
                PWS_Bluetooth.mmOutputStream.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
