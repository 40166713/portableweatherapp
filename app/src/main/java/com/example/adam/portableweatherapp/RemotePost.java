package com.example.adam.portableweatherapp;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Base64;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by adam on 12/03/15.
 *
 * Remote Fetch based on:
 * http://hmkcode.com/android-send-json-data-to-server/
 *
 * Posts data to Open Weather Map server using my own credentials
 * in final version of app these credentials will have to be swapped for user ones
 */
public class RemotePost {
    //address of OWM server post
    private static final String OPEN_WEATHER_MAP_API_POST = "http://openweathermap.org/data/post";
    //varaibles to post
    private static final String TEMPERATURE = "temp";
    private static final String HUMIDITY = "humidity";
    private static final String PRESSURE = "pressure";
    private static final String LATITUDE = "lat";
    private static final String LONGITUDE = "long";
    private static final String ALTITUDE = "alt";
    private static final String NAME = "name";
    //credentials
    private static final String USERNAME = "adam.kisala";
    private static final String PASSWORD = "dzikson6";

    private Context myContext;

    //constructor
    RemotePost(Context context){
        this.myContext = context;
    }

    //method to post data to the OWM server
    public String POST(PWS_Data pws_data){
        InputStream inputStream = null;
        String result = "";

        try{
            //basic authentication as per instructions on OWM website
            String base64EncodedCredentials = "Basic " + Base64.encodeToString(
                    (USERNAME + ":" + PASSWORD).getBytes(),
                    Base64.NO_WRAP);

            //create http client
            HttpClient httpClient = new DefaultHttpClient();

            //create post request to given url
            HttpPost httpPost = new HttpPost(OPEN_WEATHER_MAP_API_POST);
            httpPost.setHeader("Authorization", base64EncodedCredentials);
            httpPost.setHeader(HTTP.CONTENT_TYPE,"application/json");

            //create json object
            String json = "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate(TEMPERATURE, pws_data.getTemperature());
            jsonObject.accumulate(HUMIDITY, pws_data.getHumidity());
            jsonObject.accumulate(PRESSURE, converthPatoPa(pws_data.getPressure()));
            jsonObject.accumulate(LATITUDE, pws_data.getLatitude());
            jsonObject.accumulate(LONGITUDE, pws_data.getLongitude());
            jsonObject.accumulate(ALTITUDE, pws_data.getAltitude());
            jsonObject.accumulate(NAME, "PWAv1.0");


            //convert Json object to Json String
            json = jsonObject.toString();

            //set json string entity
            StringEntity stringEntity = new StringEntity(json);

            //set post entity
            httpPost.setEntity(stringEntity);

            //execute post request
            HttpResponse httpResponse = httpClient.execute(httpPost);
            //receive response
            inputStream = httpResponse.getEntity().getContent();

            //convert input stream to string
            if (inputStream != null){
                result = convertInputStreamToString(inputStream);
            }else{
                result = "Did not work";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    //convert response to string
    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null){
            result += line;
        }
        inputStream.close();
        return result;
    }

    //just a simple method to convert stored pressure in hPa to Pa as required by the server
    private String converthPatoPa(String pressure){
        double press = Double.parseDouble(pressure);
        return String.valueOf(press * 100);
    }

    //check if network is available before every post
    public boolean isConnected(){
        ConnectivityManager connectivityManager = (ConnectivityManager)myContext.getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
