package com.example.adam.portableweatherapp;

import android.os.Bundle;
import android.view.MenuItem;

/**
 * Created by adam on 14/03/15.
 * About activity, holds information about the app
 */
public class About extends BaseClass {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle(getString(R.string.about));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.about_activity;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
