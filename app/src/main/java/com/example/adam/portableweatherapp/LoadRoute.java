package com.example.adam.portableweatherapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

/**
 * Created by adam on 26/02/15.
 *
 * An activity that open the system directory and lets user to pick and load gpx file onto the map
 *
 * loadFileList() method that opens file storage and sets list adapter inspired by:
 * https://github.com/mburman/Android-File-Explore/tree/master/FileExplore
 */
public class LoadRoute extends BaseClass implements View.OnClickListener{

    // Stores names of traversed directories
    private final ArrayList<String> str = new ArrayList<String>();

    // Check if the first level of the directory structure is the one showing
    private Boolean firstLvl = true;
    private Boolean isSelected = false;

    //tag
    private static final String TAG = "F_PATH";

    //file and directories variables
    private Item[] fileList;
    private static File path = new File(Environment.getExternalStorageDirectory() + "");
    private String chosenFile;
    private String filePath;
    public static final String GPX_FILE_PATH = "file_path";
    private Handler handler;

    //list adapter for list view
    private static ListAdapter adapter;
    //list view for dir and files
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle(getString(R.string.load_route));

        //handler for posting data into UI
        handler = new Handler();
        //get external storage path
        path = new File(Environment.getExternalStorageDirectory() + "");
        //set up UI
        listView = (ListView) findViewById(R.id.files_list_view);
        listView.setOnItemClickListener(new ItemClickListener());


        //load directories and files into the file list
        loadFileList();

        listView.setAdapter(adapter);
        listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        Log.d(TAG, path.getAbsolutePath());

    }

    //method that loads directories and files into the list
    private void loadFileList() {
        try {
            path.mkdirs();
        } catch (SecurityException e) {
            Log.e(TAG, "unable to write on the sd card ");
        }

        // Checks whether path exists
        if (path.exists()) {
            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File dir, String filename) {
                    File sel = new File(dir, filename);
                    String lowerCase = filename.toLowerCase();
                    // Filters based on whether the file is hidden or not
                    return (lowerCase.endsWith(".gpx") || sel.isDirectory())
                            && !sel.isHidden();
                }
            };

            //initialize file list
            String[] fList = path.list(filter);

            fileList = new Item[fList.length];
            //loop to set aside directories from files
            for (int i = 0; i < fList.length; i++) {
                fileList[i] = new Item(fList[i], R.mipmap.ic_file_black_48dp);

                // Convert into file path
                File sel = new File(path, fList[i]);

                // Set drawables
                if (sel.isDirectory()) {
                    fileList[i].icon = R.mipmap.ic_folder_open_black_48dp;
                    Log.d("DIRECTORY", fileList[i].file);
                } else {
                    Log.d("FILE", fileList[i].file);
                }
            }

        } else {
            Log.e(TAG, "path does not exist");
        }

        //assign adapter
        adapter = new ArrayAdapter<Item>(this,
                android.R.layout.select_dialog_item, android.R.id.text1,
                fileList) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                // creates view
                View view = super.getView(position, convertView, parent);
                TextView textView = (TextView) view
                        .findViewById(android.R.id.text1);

                // put the image on the text view
                textView.setCompoundDrawablesWithIntrinsicBounds(
                        fileList[position].icon, 0, 0, 0);

                // add margin between image and text (support various screen
                // densities)
                int dp5 = (int) (15 * getResources().getDisplayMetrics().density + 0.5f);
                textView.setCompoundDrawablePadding(dp5);

                return view;
            }
        };
    }

    //on click listener
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.load_route_cancel_button:     //finish activity
                this.finish();
                break;
            case R.id.load_route_load_button:       //load file into the map activity
                if (isSelected){                    //check if file selected
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Intent intent = new Intent(LoadRoute.this, MyMap.class);
                            intent.putExtra(GPX_FILE_PATH, filePath);
                            startActivity(intent);
                        }
                    }, 300);
                }
                break;
            default:
                //do nothing at the moment
                break;
        }
    }

    //method that opens up directory if directory clicked or gets a file if file selected
    private class ItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            chosenFile = fileList[position].file;
            File sel = new File(path + "/" + chosenFile);

            //if directory open up reset file list load list into list view
            if (sel.isDirectory()){

                firstLvl = false;
                // Adds chosen directory to list
                str.add(chosenFile);
                fileList = null;
                path = new File(sel + "");

                Log.d("DIRECTORY", path.toString());
                loadFileList();

                listView.setAdapter(adapter);
                isSelected = false;

            }
            // File picked
            else {
                // Perform action with file picked
                //set the item highlighted
                listView.setItemChecked(position, true);
                filePath = sel.getPath();
                Log.d("FILE", filePath);
                isSelected = true;
            }
            //set toolbar title accordingly to the first level position
            setToolbar(firstLvl);
        }

    }

    //method that sets toolbar title depending on directory in
    private void setToolbar(Boolean firstLvl){
        if (firstLvl) {
            setToolbarTitle(getString(R.string.load_route));
        }else{
            setToolbarTitle(str.get(str.size() - 1));
        }
    }

    //private class that represents line of directory and title or file and title
    private class Item {
        public final String file;
        public int icon;

        public Item(String file, Integer icon) {
            this.file = file;
            this.icon = icon;
        }

        @Override
        public String toString() {
            return file;
        }
    }

    //set content view
    @Override
    protected int getLayoutResource() {
        return R.layout.load_route;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (firstLvl){
                    this.finish();
                }
                //if not parent directory then go back up in the directory hierarchy
                else{
                    // present directory removed from list
                    String s = str.remove(str.size() - 1);
                    // path modified to exclude present directory
                    path = new File(path.toString().substring(0,
                            path.toString().lastIndexOf(s)));
                    fileList = null;
                    // if there are no more directories in the list, then
                    // its the first level
                    if (str.isEmpty()) {
                        firstLvl = true;
                    }
                    loadFileList();
                    setToolbar(firstLvl);
                    listView.setAdapter(adapter);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
