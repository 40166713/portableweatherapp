package com.example.adam.portableweatherapp;

/**
 * Created by adam on 10/03/15.
 *
 * An abstract class recommended by Google Play Services Location Services tutorial
 * enforces onLocationChanged() method
 */
public abstract class FusedLocationReceiver {
    public abstract void onLocationChanged();
}
