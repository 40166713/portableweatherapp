package com.example.adam.portableweatherapp;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

/**
 * Created by adam on 05/03/15.
 * PWS_Bluetooth is a Service class that runs in the foreground (so it doesn't get killed by Android OS)
 * it establishes connection with PWS device and fetches data through input stream, saves that data into
 * the file every 40th correct read and uploads to the OWM server if network state available
 * Creates ongoing notification while running
 */
public class PWS_Bluetooth extends Service {

    private static String jSonString;
    private static FileHelper fileHelper;
    private static int counter = 0;
    //divider does module division to upload only every 40th correct read - every 4 minutes
    private static final int DIVIDER = 40;

    //error code variables
    private static final boolean ERROR_CODE = true;
    public static final String PARAM_BOOL_OUT = "bool_out";

    //variables for BT connection
    private BluetoothAdapter bluetoothAdapter = null;
    public static final String PARAM_OUT_MSG = "omsg";
    static final String CLICKED_CONNECT = "click_connect";
    static boolean clicked_connect_bluetooth = false;
    public static int FOREGROUND_SERVICE = 101;
    private BluetoothSocket bluetoothSocket;
    public static OutputStream mmOutputStream;
    private InputStream mmInputStream;
    private UUID uuid;
    private BluetoothDevice mmDevice;

    //post properties to OWM server
    RemotePost remotePost;

    //Location manager for getting long and lat
    LocationProvider locationProvider;
    private static Location location;


    public static String data;
    private Looper mServiceLooper;

    private ServiceHandler mServiceHandler;
    //icon for notifications
    Bitmap icon;

    //Handler to receive message from the thread
    private final class ServiceHandler extends Handler{
        public ServiceHandler(Looper looper){
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            //do data fetch here
           synchronized (this){
               //set connected
               if (!PWSWeather.connected){
                   PWSWeather.connected = true;
               }
            //run until clicked disconnect
            while(true){
                //if clicked disconnect stop service and get out of the loop
                if (!clicked_connect_bluetooth || !PWSWeather.clicked_connect){

                    try {
                        closeBT();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    PWSWeather.clicked_connect = false;
                    Log.d("PWS_BLUETOOTH", "clicked Disconnect");
                    stopForeground(true);
                    stopSelf();
                    break;
                }
                try {
                    //try reading data from input stream
                    final byte delimiter = 10; //newline character
                    int readBufferPosition = 0;
                    byte[] readBuffer = new byte[1024];
                    int bytesAvailable = mmInputStream.available();
                    //if no bytes available wait for a second and check again
                    while((bytesAvailable <= 0)){
                         //wait for a second, get a read again
                        wait(1000);
                        //checked if 'Disconnect' clicked
                        if (!PWSWeather.clicked_connect){
                            break;
                        }
                        bytesAvailable = mmInputStream.available();
                    }
                    //if bytes available get them into string
                    if (bytesAvailable > 0){
                        byte[] packetBytes = new byte[bytesAvailable];
                        mmInputStream.read(packetBytes);
                        byte startByte =  packetBytes[0];
                        //check if start byte is 'T' = 84 in decimal - start of transfer
                        if (startByte == 84){
                            //read bytes until newline
                            for (int i = 0; i < bytesAvailable; i++){
                                byte b = packetBytes[i];
                                //new line reached
                                if (b == delimiter){
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    //that's our data
                                    data = new String(encodedBytes, "US-ASCII");
                                    //reset buffer position
                                    readBufferPosition = 0;

                                    // sending data over as a broadcast
                                    Intent broadcastIntent = new Intent();
                                    broadcastIntent.setAction(PWSWeather.ResponseReceiver.ACTION_RESP);
                                    broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
                                    broadcastIntent.putExtra(PARAM_OUT_MSG, data);
                                    sendBroadcast(broadcastIntent);

                                    //get location THIS IS WHERE I START PUTTING DATA INTO FILE
                                    // ONCE DATA HAS BEEN FETCHED WITHOUT ERRORS AND LOCATION IS AVAILABLE
                                    if (location != null){
                                        //increment counter
                                        counter++;
                                        //check if that is the 40th time correct read is taken
                                        if (counter % DIVIDER == 0){
                                            //get data and put it into file
                                            double latitude = location.getLatitude();
                                            double longitude = location.getLongitude();
                                            //start saving data into the file
                                            PWS_Data pws_data = new PWS_Data(
                                                    PWS_Data.getTemperatureFromData(data),
                                                    PWS_Data.getHumidityFromData(data),
                                                    PWS_Data.getPressureFromData(data),
                                                    PWS_Data.getAltitudeFromData(data),
                                                    String.valueOf(latitude),
                                                    String.valueOf(longitude)
                                            );
                                            //get data to JSON String format and append it to file
                                            Gson gson = new Gson();
                                            jSonString = gson.toJson(pws_data);
                                            fileHelper.appendToFile(jSonString + ",");
                                            Log.d("PWS_BLUETOOTH", PWS_Data.timestampToHours(pws_data.getMyTimestamp()));
                                            //post data to OWM server if connection available 
                                            if(remotePost.isConnected()){
                                                String result = remotePost.POST(pws_data);
                                                Log.d("PWS_BLUETOOTH", result);
                                            }
                                        }
                                        //reset counter so it doesn't overflow the defined integer
                                        if(counter > 1000){
                                            counter = 0;
                                        }
                                    }else{ //if location == null
                                        Log.d("PWS_BLUETOOTH", "Location not available");
                                        location = locationProvider.getLocation();
                                    }
                                    Log.d("PWS_BLUETOOTH", "fetched data " + data);
                                }else{  //if byte != new line
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                        else{
                            //if packet doesn't start with 'T' - read again
                            Log.d("PWS_BLUETOOTH", "doesn't start with 'T' " + startByte);
                        }
                    }
                } catch (Exception e) {
                    stopServiceDisconnect();
                    e.printStackTrace();
                    break;
                }
            }
          }

        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        //start service in the new thread so it doesn't run on UI thread
        HandlerThread thread = new HandlerThread("ServiceStartArguments",
                Process.THREAD_PRIORITY_FOREGROUND);
        thread.start();

        //check for google services
        if (!isGoogleServicesAvailable()){
            Log.d("PWS_Bluetooth", "Google not available");
        }else{
            Log.d("PWS_Bluetooth", "Google connected");
            //instantiate LocationProvider
            locationProvider = new LocationProvider(this, new FusedLocationReceiver(){

                @Override
                public void onLocationChanged(){
                    Log.d("PWS_Bluetooth", "location changed");
                    location = locationProvider.getLocation();
                }
            });
        }

        //remote post initialise
        remotePost = new RemotePost(this);

        /**************************BLUETOOTH SET UP*****************************************/
        //get default bt adapter
        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
        Log.d("PWS_BLUETOOTH", "inside on create()");
    }

    //method to check if google services are available
    private boolean isGoogleServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status){
            return true;
        }else{
            Log.d("PWS_BLUETOOTH", GooglePlayServicesUtil.getErrorString(status));
            return false;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    //method called after onCreate but before Handle Message
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("PWS_BLUETOOTH", "inside on startCommand");

        //get boolean from previous activity
        if (intent != null){
            intent.getExtras();
            clicked_connect_bluetooth = intent.getBooleanExtra(CLICKED_CONNECT, false);
        }

        //initialize bluetooth connection
        try {
            findBT();
            uuid = mmDevice.getUuids()[0].getUuid();
            bluetoothSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
            bluetoothSocket.connect();
            mmOutputStream = bluetoothSocket.getOutputStream();
            mmInputStream = bluetoothSocket.getInputStream();
        } catch (IOException e) {
            stopServiceDisconnect();
            e.printStackTrace();
        }

        //initialise file helper
        fileHelper = new FileHelper(PWS_Data.PWS_DATA_FILENAME, this);

        // For each start request, send a message to start a job and deliver the
        // start ID so we know which request we're stopping when we finish the job
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        //initialize ongoing notification
        Intent notificationIntent = new Intent(this, PWSWeather.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

         icon = BitmapFactory.decodeResource(getResources(),
                R.mipmap.ic_logo);

        Notification notification = new NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setTicker(getString(R.string.app_name))
                .setContentText("Collecting data")
                .setLargeIcon(Bitmap.createScaledBitmap(icon, 128, 128, false))
                .setContentIntent(pendingIntent)
                .setOngoing(true).build();
        startForeground(FOREGROUND_SERVICE,
                notification);

        // If we get killed, after returning from here, start again
        return START_STICKY;
    }

    //if user kills app stop service
    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("PWS_BLUETOOTH", "inside onDestroy");
        if (PWSWeather.clicked_connect){
            PWSWeather.clicked_connect = false;
            // sending data over as a broadcast
            Intent broadcastIntent = new Intent();
            broadcastIntent.setAction(PWSWeather.ResponseReceiver.ACTION_RESP);
            broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
            broadcastIntent.putExtra(PARAM_BOOL_OUT, ERROR_CODE);
            sendBroadcast(broadcastIntent);
        }
        try {
            closeBT();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //method to disconnect service and bluetooth
    private void stopServiceDisconnect(){
        if (PWSWeather.connected) {
            PWSWeather.connected = false ;
        }
        // sending data over as a broadcast
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(PWSWeather.ResponseReceiver.ACTION_RESP);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(PARAM_BOOL_OUT, ERROR_CODE);
        sendBroadcast(broadcastIntent);
        Log.d("PWS_BLUETOOTH", "exception caught in message handler");

        try {
            closeBT();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        stopForeground(true);
        stopSelf();
    }

    //method that closes bt connection, sockets and streams
    private void closeBT() throws Exception {
        if (PWSWeather.connected){
            PWSWeather.connected = false;
        }
       try{
           bluetoothAdapter = null;
           mmOutputStream.close();
           mmInputStream.close();
           bluetoothSocket.close();
       }catch(Exception e){
           e.printStackTrace();
       }

    }

    //method to locate HC-06 bluetooth device
    private void findBT(){
        Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            Log.d("BLUETOOTH", "pairedDevices > 0");
            for (BluetoothDevice device : pairedDevices) {
                if (device.getName().equals("HC-06")) {
                    mmDevice = device;
                    Log.d("BLUETOOTH", "found arduino bluetooth");
                    break;
                }
            }
        }
    }
}
