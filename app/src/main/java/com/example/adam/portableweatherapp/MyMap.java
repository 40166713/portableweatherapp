package com.example.adam.portableweatherapp;

import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

/**
 * Created by adam on 26/02/15.
 *
 * MyMap class created using:
 *      https://developers.google.com/maps/documentation/android/start
 *      https://developers.google.com/maps/documentation/android/map
 *      https://google-developers.appspot.com/maps/documentation/android/maps-in-action
 *      http://codebybrian.com/2012/12/06/google_maps_android_v2_sample.html
 *
 * Simple Google Maps Activity with menu for different map views and Location button enabled
 */
public class MyMap extends BaseClass implements OnMapReadyCallback {

    //UI elements
    private static MapDetails mapDetails;
    private String[] mDrawerItems;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    private GoogleMap googleMap;
    private Polyline polyline;
    private List<Location> gpxList;
    private LatLng latLng;
    private final float cameraZoom = 15;

    //file to store map details and file helpers
    private static final String MAP_FILENAME = "map_details";
    private static final String MAP_ROUTE = "map_route";

    //file helpers to save map state
    private FileHelper fileHelper;
    private FileHelper fileHelperRoute;
    private String jsonGson;
    private android.os.Handler handler;
    private String selectedItem;

    //gpx file intent and filepath
    private Intent loadFileIntent;
    private String filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setToolbarTitle(getString(R.string.map));
        setActionBarIcon(R.mipmap.ic_menu_white_48dp);

        //instantiate file helper passing MAP_FILENAME and context
        fileHelper = new FileHelper(MAP_FILENAME, this);
        fileHelperRoute = new FileHelper(MAP_ROUTE, this);

        //initialise handler for thread activities
        handler = new android.os.Handler();

        //get intent
        loadFileIntent = this.getIntent();

        /************************************* UI SET UP*****************************************/
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ListView mDrawerList = (ListView) findViewById(R.id.drawer_list);
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.setDrawerShadow(R.color.colorPrimaryDark, Gravity.START);
        mDrawerItems = getResources().getStringArray(R.array.drawer_items_array);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.item_row, mDrawerItems));
        //set listener on drawer
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        //instantiate drawer toggle to respond to click
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,                           /* host Activity */
                drawer,                         /* DrawerLayout object */
                toolbar,                       /* nav drawer icon to replace 'Up' caret */
                R.string.open_drawer,                          /* "open drawer" description */
                R.string.close_drawer                           /* "close drawer" description */
        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        // Set the drawer toggle as the DrawerListener
        drawer.setDrawerListener(mDrawerToggle);

        /** Initialize Map**/
        try{
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    initaliseMap();
                }
            }, 200);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method to load map. If map is not created it will create it
     * */
    private void initaliseMap() {
        if (googleMap == null){
            MapFragment mapFragment = (MapFragment) getFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);
        }
    }

    /**
     * getMapAsync() callback, instantiates Map with set parameters
     **/
    @Override
    public void onMapReady(GoogleMap googleMaps) {
        googleMap = googleMaps;
        googleMap.setMyLocationEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setCompassEnabled(true);

        //if map state file exists, restore map to the same state as last time
        if (loadFileIntent != null && loadFileIntent.hasExtra(LoadRoute.GPX_FILE_PATH)){
            if (fileHelperRoute.fileExistence(MAP_ROUTE)){
                //fileHelperRoute.deleteFile(MAP_ROUTE);
                Log.d("MAP_FILE", "deleting file");
            }
            Log.d("MAP_FILE", "loading gpx file");
            handler.post(new Runnable() {
                @Override
                public void run() {
                    loadGPXFile();
                }
            });
        //if not an intent and state map was saved load it
        }
        else if (fileHelper.fileExistence(MAP_FILENAME)){
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            readMapState();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    //if map was previously loaded, get it again
                                    if (fileHelperRoute.fileExistence(MAP_ROUTE)){
                                        Gson gson = new Gson();
                                        jsonGson = fileHelperRoute.readFromFile(MAP_ROUTE);
                                        Type datasetListType = new TypeToken<Collection<Location>>() {}.getType();
                                        gpxList = gson.fromJson(jsonGson, datasetListType);
                                        drawPolylineOnMap(gpxList);
                                    }
                                }
                            }, 50);
                        }
                    });
        }
        //else just zoom into your location,
        else{
            googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
                @Override
                public void onMyLocationChange(Location location) {
                    latLng = new LatLng(location.getLatitude(),location.getLongitude());
                    if (googleMap != null){
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, cameraZoom));
                            }
                        }, 200);
                    }
                }
            });
        }
    }

    //method to load pgx file onto the map
    private void loadGPXFile() {
        try{
            //get file
            filePath = (String) loadFileIntent.getExtras().get(LoadRoute.GPX_FILE_PATH);
            if (filePath != null){
                Log.d("MAP_FILE", filePath);
                File file  = new File(filePath);
                //using GPX_Parser class
                GPX_Parser parser = new GPX_Parser();
                //decode file depending on the tag for lat and long
                gpxList = parser.decodeGPX(file, GPX_Parser.RTEPT);
                //if RTEPT tag null then try TRKPT
                if (gpxList.isEmpty()){
                    gpxList = parser.decodeGPX(file, GPX_Parser.TRKPT);
                    drawPolylineOnMap(gpxList);
                    zoomInOnFirstFileLoad(new LatLng(gpxList.get(0).getLatitude(), gpxList.get(0).getLongitude()));
                }
                else{
                    drawPolylineOnMap(gpxList);
                    zoomInOnFirstFileLoad(new LatLng(gpxList.get(0).getLatitude(), gpxList.get(0).getLongitude()));
                }
                //write a list of map pointers to the file so it can be easily retrieved later on
                Gson gson = new Gson();
                jsonGson = gson.toJson(gpxList);
                fileHelperRoute.writeToFile(jsonGson);
            }
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }
    //draw lines on map with the list from pgx file
    private void drawPolylineOnMap(List<Location> gpxList){

        final LatLng startingPoint = new LatLng((gpxList.get(0).getLatitude()), gpxList.get(0).getLongitude());
        PolylineOptions polylineOptions = new PolylineOptions();
        for (Location location : gpxList){
            polylineOptions.add(new LatLng(location.getLatitude(), location.getLongitude()));
        }
        googleMap.addMarker(new MarkerOptions().position(startingPoint).draggable(false));
        polyline = googleMap.addPolyline(polylineOptions);
        polyline.setGeodesic(true);
    }

    //method to zoom in on the first map load
    private void zoomInOnFirstFileLoad(final LatLng latLng){
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11));
                Log.d("MAP_FILE", "should've zoomed already to " + latLng);
            }
        }, 250);
    }

    //set content view
    @Override
    protected int getLayoutResource() {
        return R.layout.my_map;
    }

    //inflate menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //get different map views on click
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to setting menu clicks which consist map types
            case R.id.normal_map:
                googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
                return true;
            case R.id.terrain_map:
                googleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
                return true;
            case R.id.satellite_map:
                googleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
                return true;
            case R.id.hybrid_map:
                googleMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /** On resume initialise map to either saved state or your location**/
    @Override
    protected void onResume() {
        super.onResume();
        initaliseMap();
    }
    /** On pause save map state into the file**/
    @Override
    protected void onPause() {
        super.onPause();
         saveMapState();

    }

    /** On drawer click listener that fires new activities and saves map state before going to new activity**/
    private class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            drawer.closeDrawer(Gravity.START);                      //close drawer
            selectedItem = mDrawerItems[position];                  //get string from selected drawer item

            //save map state before closing activity
            new AsyncTask<Object, Object, Object>(){
                @Override
                protected Object doInBackground(Object[] params) {
                    saveMapState();
                    return null;
                }
            };
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //if-else statement to launch other activities
                    if (selectedItem.equalsIgnoreCase(getString(R.string.real_time_data))) {
                        Intent intent = new Intent(MyMap.this, PWSWeather.class);
                        startActivity(intent);
                        MyMap.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.forecast))) {
                        Intent intent = new Intent(MyMap.this, Forecast.class);
                        startActivity(intent);
                        MyMap.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.graphs))) {
                        Intent intent = new Intent(MyMap.this, Graphs.class);
                        startActivity(intent);
                        MyMap.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.map))) {
                        //do nothing here at the moment
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.load_route))) {
                        Intent intent = new Intent(MyMap.this, LoadRoute.class);
                        startActivity(intent);
                        MyMap.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.about))) {
                        Intent intent = new Intent(MyMap.this, About.class);
                        startActivity(intent);
                    }
                }
            }, 300);

        }
    }

    /** Method that saves map state into file using private inner class MapDetails, Gson and Filehelper.class**/
    private void saveMapState() {
        int mapType = googleMap.getMapType();
        CameraPosition cameraPosition = googleMap.getCameraPosition();
        mapDetails = new MapDetails(mapType, cameraPosition);
        Gson gson = new Gson();
        jsonGson = gson.toJson(mapDetails);
        fileHelper.writeToFile(jsonGson);
    }
    /** Method that reads out map state from file using again Gson, MapDetails class and FileHelper**/
    private void readMapState(){
        Gson gson = new Gson();
        jsonGson = fileHelper.readFromFile(MAP_FILENAME);
        mapDetails = gson.fromJson(jsonGson, MapDetails.class);
        googleMap.setMapType(mapDetails.MapType);
        googleMap.moveCamera(CameraUpdateFactory.newCameraPosition(mapDetails.MyCameraPostion));
    }
    /** Private inner class that gathers required map details to save and restore**/
    private class MapDetails{
        final int MapType;
        final CameraPosition MyCameraPostion;
        MapDetails(int mapType, CameraPosition cameraPosition){
            MapType = mapType;
            MyCameraPostion = cameraPosition;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



}
