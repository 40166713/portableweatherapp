package com.example.adam.portableweatherapp;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.example.adam.portableweatherapp.charts.ChartItem;
import com.example.adam.portableweatherapp.charts.LineChartItem;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.Utils;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by adam on 26/02/15.
 * Material Design Dialog library: https://github.com/afollestad/material-dialogs
 * Graphs taken from: https://github.com/PhilJay/MPAndroidChart
 * Copyright 2014 Philipp Jahoda

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and limitations under the License.
 */

public class Graphs extends BaseClass{

    //tag
    private static final String TAG = "Graphs";

    //UI elements
    private String[] mDrawerItems;
    private DrawerLayout drawer;
    private Toolbar toolbar;
    MenuItem deleteItem;
    private String selectedItem;

    //handler for posting data not UI
    private Handler handler;

    //array list to hold PWS_Data objects
    private static ArrayList<PWS_Data> pwsDataArrayList;
    //file helper to read the file into list
    private static FileHelper fileHelper;
    //JSON array
    private static JSONArray jsonArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set up toolbar
        setToolbarTitle(getString(R.string.graphs));
        setActionBarIcon(R.mipmap.ic_menu_white_48dp);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        //initialise handler
        handler = new Handler();
        //initialise fileHelper
        fileHelper = new FileHelper(PWS_Data.PWS_DATA_FILENAME, this);
        //read file into the list
        pwsDataArrayList = new ArrayList<>();
        pwsDataArrayList = readFileIntoTheList();

        /************************************* UI SET UP*****************************************/
        //code for list view and drawer items
        ListView mDrawerList = (ListView) findViewById(R.id.drawer_list);
        drawer = (DrawerLayout) findViewById(R.id.drawer);
        drawer.setDrawerShadow(R.color.colorPrimaryDark, Gravity.START);

        mDrawerItems = getResources().getStringArray(R.array.drawer_items_array);
        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.item_row, mDrawerItems));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(
                this,                           /* host Activity */
                drawer,                         /* DrawerLayout object */
                toolbar,                       /* nav drawer icon to replace 'Up' caret */
                R.string.open_drawer,                          /* "open drawer" description */
                R.string.close_drawer                           /* "close drawer" description */
        ) {
            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

        };

        // Set the drawer toggle as the DrawerListener
        drawer.setDrawerListener(mDrawerToggle);



        /****************************Initialise Graphs *******************************/
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
              generateCharts();
            }
        }, 300);


    }


    //method from MPAndroidCharts to generate charts based on readings from file
    private void generateCharts(){
        ListView listView = (ListView) findViewById(R.id.listView1);
        //required by library
        Utils.init(getResources());

        Log.d(TAG, "pwsArrayList size: " + pwsDataArrayList.size());

        LineChartItem temperatureChart = new LineChartItem(generateDataLine(getMeasurementTemperature(), getString(R.string.celsius)), this, getString(R.string.temperature));
        LineChartItem pressureChart = new LineChartItem(generateDataLine(getMeasurementPressure(), getString(R.string.hPa)), this, getString(R.string.pressure));
        LineChartItem humidityChart = new LineChartItem(generateDataLine(getMeasurementHumidity(), getString(R.string.percentege)), this, getString(R.string.humidity));
        LineChartItem altitudeChart = new LineChartItem(generateDataLine(getMeasurementAltitude(), getString(R.string.meters)), this, getString(R.string.altitude));

        //list of charts
        ArrayList<ChartItem> list = new ArrayList<ChartItem>();
        list.add(temperatureChart);
        list.add(pressureChart);
        list.add(humidityChart);
        list.add(altitudeChart);

        //set up list view
        ChartDataAdapter chartDataAdapter = new ChartDataAdapter(this, list);
        listView.setAdapter(chartDataAdapter);

    }

    //read file with Json objects array into the list
    private ArrayList<PWS_Data> readFileIntoTheList() {
        ArrayList<PWS_Data> dataList = new ArrayList<>();
        if (fileHelper.fileExistence(PWS_Data.PWS_DATA_FILENAME)){
            String jSONString =  fileHelper.readFromFile(PWS_Data.PWS_DATA_FILENAME);
            jSONString = "[" + jSONString + "]";
            try {
                jsonArray = new JSONArray(jSONString);
                for (int i = 0; i < jsonArray.length() -1; i++){
                    Gson gson = new Gson();
                    String jsonString = String.valueOf(jsonArray.getJSONObject(i));
                    PWS_Data pws_data = gson.fromJson(jsonString, PWS_Data.class);
                    dataList.add(pws_data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "jsonString: " + jSONString);
            Log.d(TAG, String.valueOf(jsonArray.length()) + " " + String.valueOf(dataList.size()));
        }

        return dataList;
    }

    //generate line with given data set (temperature, humidity...etc)
    private LineData generateDataLine(ArrayList<Entry> dataArray, String dataSet) {

        //initialise line
        LineDataSet d1 = new LineDataSet(dataArray, dataSet);
        //line theme
        d1.setLineWidth(2.5f);
        d1.setCircleSize(4.5f);
        d1.setHighLightColor(Color.rgb(244, 117, 117));
        d1.setColor(Color.rgb(0, 77, 64));
        d1.setCircleColor(Color.rgb(0, 77, 64));
        d1.setDrawValues(false);

        ArrayList<LineDataSet> sets = new ArrayList<LineDataSet>();
        sets.add(d1);
        //return line with data set and corresponding measurement time
        LineData cd = new LineData(getMeasurementTime(), sets);
        return cd;
    }
    /** adapter that supports 4 different item types
     *  required by MPAndroidChart library*/
    private class ChartDataAdapter extends ArrayAdapter<ChartItem> {

        public ChartDataAdapter(Context context, List<ChartItem> objects) {
            super(context, 0, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getItem(position).getView(position, convertView, getContext());
        }

        @Override
        public int getItemViewType(int position) {
            // return the views type
            return getItem(position).getItemType();
        }

        @Override
        public int getViewTypeCount() {
            return 4; // we have 4 different item-types
        }
    }

    //return measurement time
    private ArrayList<String> getMeasurementTime(){
        ArrayList<String> measurementTime = new ArrayList<>();
        for (PWS_Data pws_data : pwsDataArrayList){
            measurementTime.add(PWS_Data.timestampToHours(pws_data.getMyTimestamp()) +
            "\n" + 
            PWS_Data.timestampToDayMonth(pws_data.getMyTimestamp()));
        }
        Log.d(TAG, "MeasurementTime size: " + measurementTime.size());
        return measurementTime;
    }


    //return measurement temperature
    private ArrayList<Entry> getMeasurementTemperature(){
        ArrayList<Entry> measurementTemperature = new ArrayList<>();
        for (int i = 0; i < pwsDataArrayList.size(); i++){
            PWS_Data pws_data = pwsDataArrayList.get(i);
            measurementTemperature.add(new Entry(Integer.parseInt(pws_data.getTemperature()), i));
        }
        Log.d(TAG, "MeasurementTemperature size: " + measurementTemperature.size());
        return measurementTemperature;
    }

    //return measurement humidity
    private ArrayList<Entry> getMeasurementHumidity(){
        ArrayList<Entry> measurementHumidity = new ArrayList<>();
        for (int i = 0; i < pwsDataArrayList.size(); i++){
            PWS_Data pws_data = pwsDataArrayList.get(i);
            measurementHumidity.add(new Entry((float) Double.parseDouble(pws_data.getHumidity()), i));
        }
        Log.d(TAG, "MeasurementHumidity size: " + measurementHumidity.size());
        return measurementHumidity;
    }

    //return measurement pressure
    private ArrayList<Entry> getMeasurementPressure(){
        ArrayList<Entry> measurementPressure = new ArrayList<>();
        for (int i = 0; i < pwsDataArrayList.size(); i++){
            PWS_Data pws_data = pwsDataArrayList.get(i);
            measurementPressure.add(new Entry((float) Math.round(Double.parseDouble(pws_data.getPressure()) * 10) / 10, i));
        }
        Log.d(TAG, "MeasurementPressure size: " + measurementPressure.size());
        return measurementPressure;
    }

    //return measurement altitude
    private ArrayList<Entry> getMeasurementAltitude(){
        ArrayList<Entry> measurementAltitude = new ArrayList<>();
        for (int i = 0; i < pwsDataArrayList.size(); i++){
            PWS_Data pws_data = pwsDataArrayList.get(i);
            measurementAltitude.add(new Entry((float) Math.round(Double.parseDouble(pws_data.getAltitude()) * 10) / 10, i));
        }
        Log.d(TAG, "MeasurementAltitude size: " + measurementAltitude.size());
        return measurementAltitude;
    }

    //set content view
    @Override
    protected int getLayoutResource() {
        return R.layout.graphs;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_delete_data:
                //delete file with collected data
                deleteFileWithData();
                return true;
            default:
                //nothing
        }
        return super.onOptionsItemSelected(item);
    }

    //method to show a pop up dialog in Material style, uses library
    private void deleteFileWithData(){
        new MaterialDialog.Builder(this)
                .title("Delete Weather Data")
                .content("You are about to delete all accumulated weather data.\nContinue?")
                .positiveText("Yes")
                .negativeText("No")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        getApplicationContext().deleteFile(PWS_Data.PWS_DATA_FILENAME);
                        pwsDataArrayList.clear();
                        generateCharts();
                        Log.d(TAG, "File deleted, List View invalidated.");
                    }
                }).show();

    }

    //on drawer item click listener
    private class DrawerItemClickListener implements android.widget.AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            drawer.closeDrawer(Gravity.START);                      //close drawer
            selectedItem = mDrawerItems[position];           //get string from selected drawer item

            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    //if-else statement to launch other activities
                    if (selectedItem.equalsIgnoreCase(getString(R.string.real_time_data))) {
                        Intent intent = new Intent(Graphs.this, PWSWeather.class);
                        startActivity(intent);
                        Graphs.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.forecast))) {
                        Intent intent = new Intent(Graphs.this, Forecast.class);
                        startActivity(intent);
                        Graphs.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.graphs))) {
                        //do nothing here at the moment
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.map))) {
                        Intent intent = new Intent(Graphs.this, MyMap.class);
                        startActivity(intent);
                        Graphs.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.load_route))) {
                        Intent intent = new Intent(Graphs.this, LoadRoute.class);
                        startActivity(intent);
                        Graphs.this.finish();
                    } else if (selectedItem.equalsIgnoreCase(getString(R.string.about))) {
                        Intent intent = new Intent(Graphs.this, About.class);
                        startActivity(intent);
                    }
                }
            }, 300);
        }
    }

    //inflate options menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_graphs, menu);
        deleteItem = (MenuItem) findViewById(R.id.action_delete_data);
        return super.onCreateOptionsMenu(menu);
    }
}
