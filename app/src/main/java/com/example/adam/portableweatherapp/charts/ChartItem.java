package com.example.adam.portableweatherapp.charts;

import android.content.Context;
import android.view.View;

import com.github.mikephil.charting.data.ChartData;

/**
 * Created by adam on 01/03/15.
 * Chart item class as per MPAndroidCharts
 * Abstract class that LineChartItem will extend from
 */
public abstract class ChartItem {

    static final int TYPE_LINECHART = 1;

    final ChartData<?> mChartData;

    ChartItem(ChartData<?> cd){
        this.mChartData = cd;
    }

    public abstract int getItemType();

    public abstract View getView(int position, View convertView, Context context);
}
